const form = document.getElementById('form')
const myTasks = document.getElementById('myTasks')
const addList = document.getElementById('addList')

form.addEventListener('submit', addToDo)
myTasks.addEventListener('click', deleteList)

function addToDo(event) {
    event.preventDefault()
    const newToDo = addList.value

    const li = document.createElement('li')
    myTasks.appendChild(li)
    console.log(li)

    const h5 = document.createElement('h5')
    li.appendChild(h5)
    h5.innerHTML = newToDo

    const button = document.createElement('button')
    button.classList.add('deleteButton')
    button.innerHTML = '<i class="fa-solid fa-trash"></i>'
    li.appendChild(button)
    form.reset()
}

function deleteList(event) {
    if (event.target.tagName === 'BUTTON') {
        const nearestLi = event.target.closest('li')
        nearestLi.remove()
    }
}